#!/usr/bin/env python
#Issues:
#Not entering tracking mode
#Example startup methods:

import roslib; roslib.load_manifest('ros_apm_kinect')
import serial
import rospy
from std_msgs.msg import String, Header
from sensor_msgs.msg import NavSatFix, NavSatStatus, Imu
import ros_apm_kinect.msg
import sys,struct,time,os
import math
from std_msgs.msg import String
from sensor_msgs.msg import Image
from std_msgs.msg import String
from time import time
sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)), '/opt/ros/fuerte/share/mavlink/pymavlink'))


from optparse import OptionParser
parser = OptionParser("ros_apm_kinect.py [options]")
parser = OptionParser("ros_apm_kinect.py [options]")

parser.add_option("--apm-baudrate", dest="apm_baudrate", type='int',
                  help="apm port baud rate", default=115200)
parser.add_option("--apm-device", dest="apm_device", default="None", help="serial device")
parser.add_option("--apm-stream-rate", dest="apm_stream_rate", default=20, type='int', help="requested stream rate")
parser.add_option("--gcs-device",dest="gcs_device",default="None",help="GCS Device Connection: /dev/ttyUSB0, 10.7.45.208")
parser.add_option("--gcs-device-type",dest="gcs_device_type",default="None",help="Serial, udp")
parser.add_option("--gcs-baudrate",dest="gcs_baudrate",default="57600",help="GCS Baud Rate")
parser.add_option("--enable-control",dest="enable_control", default=False, help="Enable listning to control messages")

(opts, args) = parser.parse_args()

import mavutil
import mavlinkv10

# create a mavlink serial instance
if opts.apm_device <> "None":
	apm = mavutil.mavlink_connection(opts.apm_device, baud=opts.apm_baudrate)
else:
	apm = False

if opts.gcs_device_type == "Serial":
	gcs = mavutil.mavlink_connection(opts.gcs_device,baud=opts.gcs_baudrate,source_system=1)
elif opts.gcs_device_type == "udp":
	tempstr = "udp:"+str(opts.gcs_device)+str(":14550")
	print tempstr
	gcs = mavutil.mavlink_connection(tempstr,input=False,source_system=1)
else:
  gcs = False
gcs_type = opts.gcs_device_type
print gcs
if opts.apm_device == "None":
    print("You must specify a serial device")
    sys.exit(1)

    

def send_heartbeat(m,status):
  m.mav.heartbeat_send(type=mavlinkv10.MAV_TYPE_QUADROTOR,autopilot=mavlinkv10.MAV_AUTOPILOT_GENERIC, base_mode=4, custom_mode=0, system_status=status)

def wait_heartbeat(m):
    '''wait for a heartbeat so we know the target system IDs'''
    print("Waiting for APM heartbeat")
    m.wait_heartbeat()
    print("Heartbeat from APM (system %u component %u)" % (m.target_system, m.target_system))

def send_attitude(m,time,roll,pitch,yaw,rollspeed,pitchspeed,yawspeed):
  m.mav.attitude_send(time,roll,pitch,yaw,rollspeed,pitchspeed,yawspeed)


def mav_control(m,roll,pitch,yaw,thrust):
    m.mav.set_roll_pitch_yaw_thrust_send(m.target_system, m.target_component,roll,pitch,yaw,thrust)

def send_rc(m,channels):
    m.mav.rc_channels_override_send(m.target_system, m.target_component,channels[0],channels[1],channels[2],channels[3],channels[4],channels[5],channels[6],channels[7])


#send_heartbeat(gcs,mavlinkv10.MAV_STATE_BOOT)


pub_gps = rospy.Publisher('gps', NavSatFix)
#pub_imu = rospy.Publisher('imu', Imu)
pub_rc = rospy.Publisher('rc', ros_apm_kinect.msg.RC)
pub_state = rospy.Publisher('state', ros_apm_kinect.msg.State)
pub_vfr_hud = rospy.Publisher('vfr_hud', ros_apm_kinect.msg.VFR_HUD)
pub_attitude = rospy.Publisher('attitude', ros_apm_kinect.msg.Attitude)
pub_raw_imu =  rospy.Publisher('raw_imu', ros_apm_kinect.msg.Mavlink_RAW_IMU)
rospy.Subscriber("send_rc", ros_apm_kinect.msg.RC , send_rc)
rospy.Subscriber("control", ros_apm_kinect.msg.Control , mav_control)

    
   
def initvariables():
    global cur_pitch
    global pitch_set
    global cur_yaw
    global yaw_set
    global mode
    global Y_Height
    global armed
 
    cur_pitch = 0
    cur_yaw = 0
    pitch_set = 0
    yaw_set = 0
    mode = "INITIALISING"
    armed = False

gps_msg = NavSatFix()
#class depth_converter:

#  def __init__(self):
    #cv.NamedWindow("Depth Window",3)
#    self.bridge = CvBridge()
#    self.depth_sub = rospy.Subscriber("camera/depth/image",Image,self.callback)
#  def callback(self,data):
#    try:
#      depth_image = self.bridge.imgmsg_to_cv(data, "mono16")
      #print "Depth Stuff"
#      d = cv.Get2D(depth_image,100,100)
#      print d
      #cv.ShowImage("Depth Window",depth_image)
#      cv.WaitKey(10)
#    except CvBridgeError, e:
#      print e



class image_converter:

  def __init__(self):
    global RGB_CAMERA_WINDOW_WIDTH
    global RGB_CAMERA_WINDOW_HEIGHT
    global haar
    global cv_image
    #self.image_pub = rospy.Publisher("image_topic_2",Image)
    
    #cv.NamedWindow("Blob Window",2)
    self.bridge = CvBridge()
    if opts.target_track == "motion_track":
	cv.NamedWindow("Motion Track Window",1)
    	self.image_sub = rospy.Subscriber("camera/rgb/image_color",Image,self.MotionTrack)
    elif opts.target_track == "color_track":
        
        cv.NamedWindow("Color Track Window",1)
	self.image_sub = rospy.Subscriber("camera/rgb/image_color",Image,self.ColorTrack)
    elif opts.target_track == "optical_flow":
        if opts.optic_flow_mode == "BW":
	  self.image_sub = rospy.Subscriber("camera/rgb/image_mono",Image,self.OpticalFlow)
        #elif opts.optic_flow_mode == "IR":
        #  self.image_sub = rospy.Subscriber("camera/depth/image",Image,self.OpticalFlow)
        self.mv_step = 32
        self.mv_scale = 5
        self.mv_color = (0,255,0)
        self.cflow = None
        self.flow = None
        cv.NamedWindow("Optical Flow",1)
    elif opts.target_track == "NoTrack":
	cv.NamedWindow("Camera Window",1)
    	self.image_sub = rospy.Subscriber("camera/depth/image",Image,self.ShowCamera)
	#self.image_sub = rospy.Subscriber("camera/rgb/image_color",Image,self.ShowCamera)
      

        

    self.depth_sub = rospy.Subscriber("camera/depth/image",Image,self.callbackDepth)
    self.cv_depth = None    
    cv.SetMouseCallback("Color Track Window",my_mouse_callback,cv_image)	#binds the screen,function and image	
    #haar = cv.Load('haarcascade_frontalface_alt.xml')
  


  
def mainloop():
    initvariables()
    global PITCH_SERVO_MAX
    global YAW_SERVO_MAX
    global cur_pitch
    global pitch_set
    global cur_yaw
    global yaw_set
    global mode
    global armed
    global cur_time
    init_time = time()
    cur_time = int((time() - init_time)*1000.0)
    flightmodechanged = False
    rospy.init_node('image_converter', anonymous=True)
    packet_counter = 0
    bad_packet_counter = 0
    attitude_packet_counter = 0
    j = 1000
    direction = True
    
    cur_pitch = 0
    cur_yaw = 0
    msg = ""
    while not rospy.is_shutdown():
        cur_time = int((time() - init_time)*1000.0)
        rospy.sleep(0.001)
        
	if gcs_type == "Serial" and gcs <> False:
		while gcs.port.inWaiting() > 0:
			gcs_msg = gcs.recv_msg()
		        if gcs_msg == None: break	
        if packet_counter == 100:
          packet_counter = 1
	  bad_packet_counter = 0
	  attitude_packet_counter = 0
	else:
	  packet_counter = packet_counter + 1
        if packet_counter % 50 == 0:
		
		
		if gcs <> False:
			print cur_time
			send_heartbeat(gcs,mavlinkv10.MAV_STATE_ACTIVE)
        #print 'Mode: ' + str(mode) + ' x: ' + str(int(x_center)) + ' y: ' + str(int(y_center)) + ' C_P_Angle: ' + str(int(cur_pitch*180/3.14159)) + ' S_P_Angle: ' + str(int(pitch_set*180/3.14159)) + ' C_Y_Angle: ' + str(int(cur_yaw)) + ' S_Y_Angle: ' + str(int(yaw_set*180/3.14)) + " Dist: {:.3f}".format(distance*39.37) + 'in'

         
        
        pitch_set = 0.0
        yaw_set = 0.0
	roll = 0.0
        thrust = .5
        #mav_control(apm,roll,pitch_set,yaw_set,thrust)
	#Use this to send Roll, Pitch, Yaw, Thrust Commands.  Roll, Pitch, Yaw are in radians.  Thrust range from 0-1.  If no change is desired, set individual command to -1.
	
        num = -2
	if mode == "AUTO":
		if direction:
		    j = j + 1
		else:
		    j = j - 1
		if j > 100:
		    direction = False
		elif j < 0:
		    direction = True
		a = [j,j,j,j,j,j,j,j]
		#send_rc(apm,a)
		#print j
                num = 3.14*(j/100.0)
                mav_control(apm,num,num,num,num)
                
 	  


	if gcs_type == "Serial" and gcs <> False:  
        	gcs_msg = gcs.recv_match(blocking=False)
        msg = apm.recv_match(blocking=False)
        if not msg:
            continue 
               
        if msg.get_type() == "BAD_DATA":
            
            bad_packet_counter = bad_packet_counter + 1
            if mavutil.all_printable(msg.data):
                #i = i
                sys.stdout.write(msg.data)
                sys.stdout.flush()
	  
        else: 
            msg_type = msg.get_type()
            print msg_type
            if msg_type == "RC_CHANNELS_RAW" :
                pub_rc.publish([msg.chan1_raw, msg.chan2_raw, msg.chan3_raw, msg.chan4_raw, msg.chan5_raw, msg.chan6_raw, msg.chan7_raw, msg.chan8_raw]) 
		#DPG
     	 	#print "Ch1: %d Ch2: %d Ch3: %d Ch4: %d Ch5: %d Ch6: %d Ch7: %d Ch8: %d" %(msg.chan1_raw, msg.chan2_raw, msg.chan3_raw, msg.chan4_raw, msg.chan5_raw, msg.chan6_raw, msg.chan7_raw, msg.chan8_raw)
            if msg_type == "HEARTBEAT":
                pub_state.publish(msg.base_mode & mavutil.mavlink.MAV_MODE_FLAG_SAFETY_ARMED, 
                                  msg.base_mode & mavutil.mavlink.MAV_MODE_FLAG_GUIDED_ENABLED, 
                                  mavutil.mode_string_v10(msg))
                mode = mavutil.mode_string_v10(msg)
                armed = mavutil.mavlink.MAV_MODE_FLAG_SAFETY_ARMED
		#print msg.get_type()  #Print Heartbeat so we know the system is still responding.
            if msg_type == "VFR_HUD":
                pub_vfr_hud.publish(msg.airspeed, msg.groundspeed, msg.heading, msg.throttle, msg.alt, msg.climb)

            if msg_type == "GPS_RAW_INT":
                fix = NavSatStatus.STATUS_NO_FIX
                if msg.fix_type >=3:
                    fix=NavSatStatus.STATUS_FIX
                pub_gps.publish(NavSatFix(latitude = msg.lat/1e07,
                                          longitude = msg.lon/1e07,
                                          altitude = msg.alt/1e03,
                                          status = NavSatStatus(status=fix, service = NavSatStatus.SERVICE_GPS) 
                                          ))
            #pub.publish(String("MSG: %s"%msg))
            if msg_type == "ATTITUDE" :
                attitude_packet_counter = attitude_packet_counter + 1
                pub_attitude.publish(msg.roll, msg.pitch, msg.yaw, msg.rollspeed, msg.pitchspeed, msg.yawspeed)
		cur_pitch = msg.pitch
                cur_yaw = msg.yaw*180/3.14159
                print 'Mode: ' + str(mode) + ' C_P_Angle: ' + str(int(cur_pitch*180/3.14159)) + ' C_Y_Angle: ' + str(int(cur_yaw)) + str(int(num))
		if gcs <> False:
			send_attitude(gcs,cur_time,msg.roll,msg.pitch,msg.yaw,msg.rollspeed,msg.pitchspeed,msg.yawspeed)
            #if msg_type == "LOCAL_POSITION_NED" :
                #print "Local Pos: (%f %f %f) , (%f %f %f)" %(msg.x, msg.y, msg.z, msg.vx, msg.vy, msg.vz)
	    #if msg_type == "STATUSTEXT":
            if msg_type == "RAW_IMU" :
                pub_raw_imu.publish (Header(), msg.time_usec, 
                                     msg.xacc, msg.yacc, msg.zacc, 
                                     msg.xgyro, msg.ygyro, msg.zgyro,
                                     msg.xmag, msg.ymag, msg.zmag)


    

# wait for the heartbeat msg to find the system ID
wait_heartbeat(apm)


# waiting for 10 seconds for the system to be ready
print("Sleeping for 1 seconds to allow system, to be ready")
rospy.sleep(1)
print("Sending all stream request for rate %u" % opts.apm_stream_rate)
#for i in range(0, 3):
#print mavutil.mavlink.MAV_DATA_STREAM_ALL
#print "..."
#print opts.rate
#print master.target_system
#print "..."
#print master.target_component
apm.mav.request_data_stream_send(apm.target_system, apm.target_component,                                 mavutil.mavlink.MAV_DATA_STREAM_EXTRA1, 20, 1)

if __name__ == '__main__':
    try:
        mainloop()
    except rospy.ROSInterruptException: pass
