FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/rgbdslam/msg"
  "../src/rgbdslam/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_py"
  "../src/rgbdslam/msg/__init__.py"
  "../src/rgbdslam/msg/_Position.py"
  "../src/rgbdslam/msg/_RC.py"
  "../src/rgbdslam/msg/_DataToFCGPS.py"
  "../src/rgbdslam/msg/_Control.py"
  "../src/rgbdslam/msg/_DataToFC.py"
  "../src/rgbdslam/msg/_VFR_HUD.py"
  "../src/rgbdslam/msg/_Attitude.py"
  "../src/rgbdslam/msg/_GPS.py"
  "../src/rgbdslam/msg/_Mavlink_RAW_IMU.py"
  "../src/rgbdslam/msg/_DataFromFC.py"
  "../src/rgbdslam/msg/_State.py"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_py.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
