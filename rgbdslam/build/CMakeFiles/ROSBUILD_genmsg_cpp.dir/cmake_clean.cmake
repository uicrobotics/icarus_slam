FILE(REMOVE_RECURSE
  "../msg_gen"
  "../srv_gen"
  "../src/rgbdslam/msg"
  "../src/rgbdslam/srv"
  "../msg_gen"
  "../srv_gen"
  "CMakeFiles/ROSBUILD_genmsg_cpp"
  "../msg_gen/cpp/include/rgbdslam/Position.h"
  "../msg_gen/cpp/include/rgbdslam/RC.h"
  "../msg_gen/cpp/include/rgbdslam/DataToFCGPS.h"
  "../msg_gen/cpp/include/rgbdslam/Control.h"
  "../msg_gen/cpp/include/rgbdslam/DataToFC.h"
  "../msg_gen/cpp/include/rgbdslam/VFR_HUD.h"
  "../msg_gen/cpp/include/rgbdslam/Attitude.h"
  "../msg_gen/cpp/include/rgbdslam/GPS.h"
  "../msg_gen/cpp/include/rgbdslam/Mavlink_RAW_IMU.h"
  "../msg_gen/cpp/include/rgbdslam/DataFromFC.h"
  "../msg_gen/cpp/include/rgbdslam/State.h"
)

# Per-language clean rules from dependency scanning.
FOREACH(lang)
  INCLUDE(CMakeFiles/ROSBUILD_genmsg_cpp.dir/cmake_clean_${lang}.cmake OPTIONAL)
ENDFOREACH(lang)
