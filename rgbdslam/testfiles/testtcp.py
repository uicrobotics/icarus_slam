#!/usr/bin/env python

import socket


TCP_IP = '192.168.1.104'
TCP_PORT = 9761
BUFFER_SIZE = 1024
MESSAGE = "Hello, World!"

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
while True:
	var = raw_input("")
	if var == "q":
		break
	try:
		s.connect((TCP_IP, TCP_PORT))
		s.send(MESSAGE)
		data = s.recv(BUFFER_SIZE)
		s.close()
	except:
		continue

print "received data:", data
