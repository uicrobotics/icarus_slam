(cl:in-package rgbdslam-msg)
(cl:export '(TIME-VAL
          TIME
          LATITUDE-VAL
          LATITUDE
          LONGITUDE-VAL
          LONGITUDE
          ALTITUDE-VAL
          ALTITUDE
))