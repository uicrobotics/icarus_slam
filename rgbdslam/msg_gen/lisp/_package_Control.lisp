(cl:in-package rgbdslam-msg)
(cl:export '(ROLL-VAL
          ROLL
          PITCH-VAL
          PITCH
          YAW-VAL
          YAW
          THRUST-VAL
          THRUST
))