(cl:defpackage rgbdslam-msg
  (:use )
  (:export
   "<POSITION>"
   "POSITION"
   "<RC>"
   "RC"
   "<DATATOFCGPS>"
   "DATATOFCGPS"
   "<CONTROL>"
   "CONTROL"
   "<DATATOFC>"
   "DATATOFC"
   "<VFR_HUD>"
   "VFR_HUD"
   "<ATTITUDE>"
   "ATTITUDE"
   "<GPS>"
   "GPS"
   "<MAVLINK_RAW_IMU>"
   "MAVLINK_RAW_IMU"
   "<DATAFROMFC>"
   "DATAFROMFC"
   "<STATE>"
   "STATE"
  ))

